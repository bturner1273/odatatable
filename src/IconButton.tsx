import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';

/**
 * Icon button properties
 * @interface
 */
export interface IIconButtonProps {
    text: string;
    icon: IconDefinition;
    onClick?: React.MouseEventHandler<HTMLButtonElement>;
    onMouseDown?: React.MouseEventHandler<HTMLButtonElement>;
    buttonClass?: string;
    data?: string;
}

/**
 * Represents a button with a FontAwesomeIcon, text, and an onClick function
 * @param {IIconButtonProps} props
 * @returns
 */
export const IconButton: React.FunctionComponent<IIconButtonProps> = (
    props: IIconButtonProps
) => {
    return (
        <button
            onClick={props.onClick}
            onMouseDown={props.onMouseDown}
            className={props.buttonClass}
            data-custom-button-data={props.data}
        >
            <FontAwesomeIcon icon={props.icon}></FontAwesomeIcon> {props.text}
        </button>
    );
};
export default IconButton;
