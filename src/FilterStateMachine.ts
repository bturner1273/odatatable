export const UpDownArrow = '⇳';
export const UpArrow = '⇧';
export const DownArrow = '⇩';
/**UpArrow, DownArrow, or UpDownArrow */
export type SortState = '⇳' | '⇧' | '⇩';
/**
 * Controls the sort state of an ODataTable header
 * @class
 */
export class SortStateMachine {
    private state: SortState;
    constructor() {
        this.state = UpDownArrow;
    }
    getState = (): SortState => this.state;
    transition = (): void => {
        switch (this.state) {
            case UpDownArrow:
                this.state = UpArrow;
                break;
            case DownArrow:
                this.state = UpDownArrow;
                break;
            default:
                this.state = DownArrow;
                break;
        }
    };
}
