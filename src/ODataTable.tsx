import React, { CSSProperties } from 'react';
import ReactDOM from 'react-dom';
import { SortStateMachine, UpArrow, DownArrow } from './FilterStateMachine';
import { o, OdataQuery } from 'odata';
import { IconButton, IIconButtonProps } from './IconButton';
import {
    faEdit,
    faTrash,
    faList,
    faStar,
    faCopy
} from '@fortawesome/free-solid-svg-icons';
import jsConvert from 'js-convert-case';
import Skeleton from 'react-loading-skeleton';

/**
 * Represents a text casing style. Used for mapping column titles to object keys
 * @type
 */
export type TextCasingType =
    | 'camel'
    | 'snake'
    | 'pascal'
    | 'dot'
    | 'path'
    | 'text'
    | 'sentence'
    | 'header'
    | 'lower'
    | 'upper';

/**
 * Base ODataTable CSS stylings that are not meant to change
 * @object
 */
export const ODataTableStyles: {
    [key: string]: CSSProperties;
} = {
    /**Make hover cursor make item look clickable */
    cursorPointer: {
        cursor: 'pointer'
    },
    /**Split into 2 grid columns for filtering/paging */
    cardHeader: {
        display: 'grid',
        gridTemplateColumns: '75% 25%'
    },
    /**Split into 3 grid columns for title/filtering/paging */
    titledCardHeader: {
        display: 'grid',
        gridTemplateColumns: '25% 50% 25%'
    },
    /**Make 'card-title' display inline and aligned/justified left */
    cardTitle: {
        display: 'inline',
        placeSelf: 'left'
    },
    /**mrx where x*5px is the margin */
    mr1: {
        marginRight: '5px'
    },
    /**mrx where x*5px is the margin */
    mr2: {
        marginRight: '10px'
    },
    /**mlx where x*5px is the margin */
    ml2: {
        marginLeft: '10px'
    },
    /**Center and elongate filter search div */
    filterDiv: {
        placeSelf: 'center',
        width: '100%'
    },
    /**Elongate and round filter search input */
    filterInput: {
        borderRadius: '10px',
        fontSize: 14,
        width: '100%',
        border: 'none',
        outline: 'none',
        paddingLeft: '10px'
    },
    /**Align/justify content center */
    alignJustifyCenter: {
        placeSelf: 'center'
    },
    /**Short page number input made to fit numbers up to 9999 */
    pagerInput: {
        width: '60px',
        fontSize: 14,
        textAlign: 'center',
        verticalAlign: 'middle',
        outline: 'none',
        height: '100%'
    },
    /**Border radius brx where x is numPixels */
    br10: {
        borderRadius: '10px'
    },
    /**Pager next/previous button styling */
    pagerButton: {
        border: 'none',
        textAlign: 'center',
        textDecoration: 'none',
        display: 'inline-block',
        verticalAlign: 'middle',
        outline: 'none',
        height: '100%'
    },
    /**Make ODataTable not resize columns on data change */
    oDataTableTable: {
        tableLayout: 'fixed'
    },
    /**Float right */
    floatRight: {
        float: 'right'
    },
    /**Float left */
    floatLeft: {
        float: 'left'
    },
    /**25% width */
    w25: {
        width: '25%'
    },
    linkText: {
        cursor: 'pointer',
        color: '#2780E3'
    },
    textCenter: {
        textAlign: 'center'
    },
    textGray: {
        color: 'GrayText'
    },
    height30px: {
        height: '30px',
        maxHeight: '30px'
    }
};

/**
 * Describes whether the ODataTable button column preceeds or proceeds the data columns
 * @type
 */
export type ODataTableButtonColumnPosition = 'first' | 'last';
/**
 * The type of data stored in the rows of the column
 * @type
 */
export type ODataTableColumnType =
    | 'number'
    | 'string'
    | 'boolean'
    | 'enum'
    | 'datetime';

/**
 * Interface representing a table column
 * @interface
 */
export interface IODataTableColumn {
    /**The column header value */
    name: string;
    /**The data-object property name corresponding to this column name. Default Value = IODataTableColumn.name */
    objectKey?: string;
    /**If true column header will have a sort button state-machine that toggles between ascending/descending sorts */
    sortable: boolean;
    /**If false column's associated object key will not be checked for when searching */
    searchable?: boolean;
    /**Not for component user to populate, this gets populated in ODataTable::constructor */
    index?: number;
    /**Type of data in column, this is used for determining what is filterable */
    type: ODataTableColumnType;
    /**
     * Custom enum for enum type columns, that come in from the server as numbers so they can be
     * translated into string enums for display and can be checked against for filtering
     */
    enum?: { [key: number]: string };
    /**C# Namespace of enum for odata searching/filtering */
    enumNamespace?: string;
}

/**
 * ODataTable paging options
 * @interface
 */
export interface IODataTablePagingOptions {
    /**The number of OData points to be displayed per page in the table. Default value 25 */
    pageSize?: number;
    /**Current Page. Default value 1*/
    pageNumber?: number;
    /**Page description span class. I.e. text-muted font-weight-light*/
    pagingDescriptionSpanClass?: string;
    /**Paging buttons vertical vs horizontal */
    pagingButtonsVertical?: boolean;
    /**
     * Paging buttons at top of div or bottom.
     * If this boolean is set false, it must be used
     * in conjunction with IODataTableStyleOptions.hasFooterDiv = true
     * otherwise the paging buttons will not be visible
     */
    pagingButtonsTop?: boolean;
}

/**
 * ODataTable styling options
 * @interface
 */
export interface IODataTableStyleOptions {
    /**Outer div CSS class i.e. card */
    outerDivClass?: string;
    /**Header div CSS class i.e. card-header */
    headerDivClass?: string;
    /**Title h4 CSS class i.e. card-title */
    titleClass?: string;
    /**Body div CSS class i.e. card-body */
    bodyDivClass?: string;
    /**Has footer div bool */
    hasFooterDiv?: boolean;
    /**Footer div CSS class i.e. card-footer */
    footerDivClass?: string;
    /**Table CSS class */
    tableClass?: string;
}

interface IHidableIconButtonProps extends IIconButtonProps {
    /**
     * Function that takes a data point and decides whether to show or hide the button
     */
    shouldDisplay?: (dataPoint: any) => boolean;
}

/**
 * ODataTable button column options
 * @interface
 */
export interface IODataTableButtonColumnOptions {
    /**Describes whether the button column comes first/last/is non-existent */
    position: ODataTableButtonColumnPosition;
    /**Button configurations, must be specified if a buttonColumnPosition is given */
    configurations: IHidableIconButtonProps[];
}

/**
 * ODataTable JSX Properties
 * @interface
 */
export interface IODataTableProps {
    /**OData server base url */
    indexUrl: string;
    /**
     * Tells the library how to transform column names into
     * respective object keys.
     * Example:
     * if:
     * columns[x].name="Category Name"
     * columnNameToObjectKeyStringMapping="camel"
     * then:
     * the library will look for object key "categoryName"
     * in the models sent from the server
     */
    textCasingType?: TextCasingType;
    /**Visible columns to display */
    columns: IODataTableColumn[];
    /**Multi column filter enabled */
    multiColumnFilterEnabled?: boolean;
    /**ODataTable button column options */
    buttonColumnOptions?: IODataTableButtonColumnOptions;
    /**Title */
    title: string;
    /**ODataTable styling options */
    styleOptions: IODataTableStyleOptions;
    /**ODataTable paging options */
    pagingOptions: IODataTablePagingOptions;
    /**Height of loading skeleton */
    loadingSkeletonHeight: number;
}

/**
 * ODataTable State
 * @interface
 */
export interface IODataTableState {
    /**Current page */
    pageNumber: number;
    /**Current page size, may be changable in the future */
    pageSize: number;
    /**Math.ceil(data.length / pageSize) */
    numPages: number;
    /**
     * The sort state of each sortable column i.e.
     * UpArrow, DownArrow, UpDownArrow for asc, desc, and unsorted respectively
     */
    columnSortStates: {
        [key: number]: SortStateMachine;
    };
    /**The OData points from the server */
    data: any[];
    /**Current $orderby query value */
    $orderby: string;
    /**Current $filter query value */
    $filter: string;
    /**Loaded */
    loaded: boolean;
}

/**
 * Default ODataTable props
 * @constant
 */
const defaultODataTableProps = {
    pageSize: 25,
    pageNumber: 1,
    pagingButtonsVertical: false,
    pagingButtonsTop: true,
    hasFooterDiv: false,
    multiColumnFilterEnabled: false
};

/**
 * Datatable that filters/pages/sorts itself with an odata client
 * @param {IODataTableProps} props - Table JSX attributes
 * @constructor
 */
export class ODataTable extends React.Component<
    IODataTableProps,
    IODataTableState
> {
    private _pagePickerInputRef: React.RefObject<HTMLInputElement>;
    private _filterSearchInputRef: React.RefObject<HTMLInputElement>;
    private _pagingButtonsVertical: boolean = false;
    private _pagingButtonsTop: boolean = true;
    private _hasFooterDiv: boolean = false;
    private _multiColumnFilterEnabled: boolean = false;
    private _loadingSkeletonHeight: number;
    /**
     * ODataTable constructor
     * @param {IODataTableProps} props - ODataTable properties
     */
    constructor(props: IODataTableProps) {
        super(props);

        this._pagePickerInputRef = React.createRef();
        this._filterSearchInputRef = React.createRef();

        let columnSortStates: {
            [key: number]: SortStateMachine;
        } = {};
        props.columns.forEach((c, index) => {
            c.index = index;
            if (!c.objectKey) {
                let tct = this.props.textCasingType;
                c.objectKey =
                    tct === 'camel'
                        ? jsConvert.toCamelCase(c.name)
                        : tct === 'snake'
                        ? jsConvert.toSnakeCase(c.name)
                        : tct === 'pascal'
                        ? jsConvert.toPascalCase(c.name)
                        : tct === 'dot'
                        ? jsConvert.toDotCase(c.name)
                        : tct === 'path'
                        ? jsConvert.toPathCase(c.name)
                        : tct === 'text'
                        ? jsConvert.toTextCase(c.name)
                        : tct === 'sentence'
                        ? jsConvert.toSentenceCase(c.name)
                        : tct === 'header'
                        ? jsConvert.toHeaderCase(c.name)
                        : tct === 'lower'
                        ? jsConvert.toLowerCase(c.name)
                        : tct === 'upper'
                        ? jsConvert.toUpperCase(c.name)
                        : c.name;
            }
            if (!c.searchable && c.searchable !== false) {
                c.searchable = true;
            }
            if (c.sortable) {
                columnSortStates[index] = new SortStateMachine();
            }
        });

        this._pagingButtonsVertical =
            props.pagingOptions.pagingButtonsVertical ??
            defaultODataTableProps.pagingButtonsVertical;
        this._pagingButtonsTop =
            props.pagingOptions.pagingButtonsTop ??
            defaultODataTableProps.pagingButtonsTop;
        this._hasFooterDiv =
            props.styleOptions.hasFooterDiv ??
            defaultODataTableProps.hasFooterDiv;
        this._multiColumnFilterEnabled =
            props.multiColumnFilterEnabled ??
            defaultODataTableProps.multiColumnFilterEnabled;
        this._loadingSkeletonHeight = this.props.loadingSkeletonHeight ?? 100;

        this.state = {
            pageSize:
                props.pagingOptions.pageSize ?? defaultODataTableProps.pageSize,
            pageNumber:
                props.pagingOptions.pageNumber ??
                defaultODataTableProps.pageNumber,
            numPages: 0,
            columnSortStates: columnSortStates,
            data: [],
            $orderby: '',
            $filter: '',
            loaded: false
        };
    }

    /**
     * Initialize state fully
     */
    async componentDidMount(): Promise<void> {
        this.setState({
            data: await this._oDataQuery(),
            numPages: Math.ceil(
                (await this._getDataCount()) / this.state.pageSize
            )
        });
        this.setState({ loaded: true });
    }

    /**
     * Returns the length of the data for a specific query or the index url
     * @param {string} url - optional url for finding the data size of a specific query
     * @returns {Promise<number>} - the length of the indicated or index url data
     */
    private _getDataCount = async (url?: string): Promise<number> => {
        var response = await (
            (await o(url ?? this.props.indexUrl)
                .get()
                .fetch({
                    $top: 0,
                    $count: true
                })) as Response
        ).json();
        return response['@odata.count'];
    };

    /**
     * Removes null/undefined object properties from an OdataQuery
     * @param {OdataQuery} queryParams - query object that needs to be cleaned i.e. null/undefined properties removed
     * @returns
     */
    private _cleanQueryParams = (queryParams: OdataQuery): OdataQuery => {
        for (var key in queryParams) {
            if (!queryParams[key]) {
                delete queryParams[key];
            }
        }
        return queryParams;
    };

    /**
     * Querys OData server at this.props.indexUrl including a $skip and $top
     * for paging based on the pageSize and pageNumber props
     * @param {OdataQuery} queryParams - i.e. $orderby, $filter etc.
     * @returns
     */
    private _oDataQuery = async (queryParams?: OdataQuery): Promise<any> => {
        if (queryParams) queryParams = this._cleanQueryParams(queryParams);
        return await o(this.props.indexUrl)
            .get()
            .query({
                $skip: (this.state.pageNumber - 1) * this.state.pageSize,
                $top: this.state.pageSize,
                ...queryParams
            });
    };

    /**
     * Pages ODataQuery to specified list location using state.pageSize and state.pageNumber
     * @param {number} pageNumber - the page to go to
     * @returns
     */
    private _pageTo = async (pageNumber: number): Promise<void> =>
        this.setState(
            { pageNumber: pageNumber },
            async (): Promise<void> =>
                this.setState({
                    data: await this._oDataQuery({
                        $filter: this.state.$filter,
                        $orderby: this.state.$orderby
                    })
                })
        );

    /**
     * Sets all applicable column sort states to default UpDownArrow
     */
    private _clearColumnSortStates = (): void => {
        let columnSortStatesShallowCopy = {
            ...this.state.columnSortStates
        };
        for (let key in columnSortStatesShallowCopy) {
            if (this.props.columns[key].sortable) {
                columnSortStatesShallowCopy[key] = new SortStateMachine();
            }
        }
        this.setState({
            columnSortStates: columnSortStatesShallowCopy
        });
    };

    /**
     * Filters or MultiFilters the datatable by column values when sortable column headers are clicked
     * @param {IODataTableColumn} c - the table column whose column header was clicked
     */
    private thClickHandler = async (c: IODataTableColumn): Promise<void> => {
        let columnSortStatesShallowCopy = {
            ...this.state.columnSortStates
        };
        if (c.index != null) {
            //transition filter state of clicked column header
            columnSortStatesShallowCopy[c.index].transition();
            this.setState({
                columnSortStates: columnSortStatesShallowCopy
            });
            if (!this._multiColumnFilterEnabled) {
                //set all other column filter states to default
                for (let key in columnSortStatesShallowCopy) {
                    if (Number(key) !== c.index) {
                        columnSortStatesShallowCopy[key] =
                            new SortStateMachine();
                    }
                }
                var clickedColumnFilterState =
                    columnSortStatesShallowCopy[c.index].getState();
                if (
                    clickedColumnFilterState === UpArrow ||
                    clickedColumnFilterState === DownArrow
                ) {
                    let $orderby = `${c.objectKey} ${
                        clickedColumnFilterState === UpArrow ? 'desc' : 'asc'
                    }`;
                    this.setState({
                        data: await this._oDataQuery({
                            $orderby: $orderby
                        }),
                        $orderby: $orderby
                    });
                } else {
                    this.setState({
                        data: await this._oDataQuery(),
                        $orderby: ''
                    });
                }
            } else {
                //loop through all keys that are sorted and build $orderby query accordingly
                let $orderby = '';
                let keys = Object.keys(columnSortStatesShallowCopy);
                for (let i = 0; i < keys.length; i++) {
                    let column: SortStateMachine =
                        columnSortStatesShallowCopy[keys[i]];
                    let columnState = column.getState();
                    if (columnState === UpArrow) {
                        $orderby += `${this.props.columns[i].objectKey} desc,`;
                    } else if (columnState === DownArrow) {
                        $orderby += `${this.props.columns[i].objectKey} asc,`;
                    }
                }
                //remove last comma
                $orderby = $orderby.substr(0, $orderby.length - 1);
                if ($orderby) {
                    this.setState({
                        data: await this._oDataQuery({
                            $orderby: $orderby,
                            $filter: this.state.$filter
                        }),
                        $orderby: $orderby
                    });
                } else {
                    this.setState({
                        data: await this._oDataQuery({
                            $filter: this.state.$filter
                        }),
                        $orderby: ''
                    });
                }
            }
        }
    };

    /**
     * Handles filterSearchInput keydown
     * @param {React.KeyboardEventHandler<HTMLInputElement>} e - the keypress event on filterSearchInput
     */
    private filterSearchChanged: React.ChangeEventHandler<HTMLInputElement> =
        async (e: React.ChangeEvent<HTMLInputElement>) => {
            let filterSearchValue = encodeURIComponent(
                this._filterSearchInputRef.current?.value ?? ''
            );
            if (filterSearchValue) {
                this._clearColumnSortStates();
                let $filter = '';
                this.props.columns.forEach(col => {
                    if (col.searchable) {
                        if (col.type === 'string') {
                            $filter += `contains(${col.objectKey},'${filterSearchValue}') or `;
                        } else if (
                            col.type === 'number' &&
                            !isNaN(Number(filterSearchValue))
                        ) {
                            $filter += `${col.objectKey} eq ${filterSearchValue} or `;
                        } else if (
                            col.type === 'boolean' &&
                            (filterSearchValue === 'true' ||
                                filterSearchValue === 'false')
                        ) {
                            $filter += `${col.objectKey} eq ${filterSearchValue} or `;
                        } else if (
                            col.type === 'enum' &&
                            col.enumNamespace &&
                            col.enum &&
                            (Object.keys(col.enum).includes(
                                filterSearchValue
                            ) ||
                                Object.values(col.enum).includes(
                                    filterSearchValue
                                ))
                        ) {
                            $filter += `${col.objectKey} eq ${col.enumNamespace}'${filterSearchValue}' or `;
                        }
                    }
                });

                //remove last or
                $filter = $filter.substr(0, $filter.length - 4);

                //request data with filter
                this.setState({
                    data: await this._oDataQuery({
                        $filter: $filter
                    }),
                    $filter: $filter,
                    numPages: Math.ceil(
                        (await this._getDataCount(
                            `${this.props.indexUrl}?$filter=${$filter}`
                        )) / this.state.pageSize
                    ),
                    pageNumber: 1
                });
            } else {
                this.setState({
                    data: await this._oDataQuery(),
                    $filter: '',
                    numPages: Math.ceil(
                        (await this._getDataCount()) / this.state.pageSize
                    )
                });
            }
        };

    /**
     * Sets the view to what the user saw before sorting/filtering/paging anything
     * @param {React.MouseEvent<HTMLHeadingElement>} e - the click event
     */
    private titleClick: React.MouseEventHandler<HTMLHeadingElement> = async (
        e: React.MouseEvent<HTMLHeadingElement>
    ) => {
        this.setState(
            {
                $orderby: '',
                $filter: '',
                pageNumber: 1
            },
            async () => {
                this.setState({
                    data: await this._oDataQuery(),
                    numPages: Math.ceil(
                        (await this._getDataCount()) / this.state.pageSize
                    )
                });
            }
        );
        if (this._filterSearchInputRef.current)
            this._filterSearchInputRef.current.value = '';
    };

    /**
     * Renders component
     * @returns JSXElement ODataTable
     */
    render(): JSX.Element {
        const pagingDescription = (
            <span
                className={this.props.pagingOptions.pagingDescriptionSpanClass}
                style={{
                    fontSize: 14
                }}
            >
                Page {this.state.pageNumber} of {this.state.numPages}
            </span>
        );
        const pagingButtons = this._pagingButtonsVertical ? (
            <div style={ODataTableStyles.floatRight}>
                {pagingDescription}
                <input
                    ref={this._pagePickerInputRef}
                    style={{
                        ...ODataTableStyles.pagerInput,
                        ...ODataTableStyles.ml2,
                        ...ODataTableStyles.br10
                    }}
                    value={this.state.pageNumber}
                    type='number'
                    min={1}
                    max={this.state.numPages}
                    onChange={() =>
                        this._pageTo(
                            Number(this._pagePickerInputRef.current?.value)
                        )
                    }
                />
            </div>
        ) : (
            <div style={ODataTableStyles.floatRight}>
                <div style={ODataTableStyles.floatLeft}>
                    {pagingDescription}
                </div>
                <br />
                <div
                    style={{
                        ...ODataTableStyles.floatRight,
                        ...ODataTableStyles.height30px
                    }}
                >
                    <button
                        style={{
                            ...ODataTableStyles.pagerButton
                        }}
                        onClick={() => this._pageTo(this.state.pageNumber - 1)}
                        disabled={this.state.pageNumber <= 1}
                    >
                        &lt;
                    </button>
                    <input
                        ref={this._pagePickerInputRef}
                        style={ODataTableStyles.pagerInput}
                        value={this.state.pageNumber}
                        pattern='[0-9]'
                        onChange={() =>
                            this._pageTo(
                                Number(this._pagePickerInputRef.current?.value)
                            )
                        }
                    />
                    <button
                        style={{
                            ...ODataTableStyles.pagerButton
                        }}
                        onClick={() => this._pageTo(this.state.pageNumber + 1)}
                        disabled={this.state.pageNumber >= this.state.numPages}
                    >
                        &gt;
                    </button>
                </div>
            </div>
        );

        const generateButtonColumnButtons = (
            options: IODataTableButtonColumnOptions,
            dataPoint: any
        ) => {
            if (
                this.props.buttonColumnOptions !== null &&
                typeof this.props.buttonColumnOptions !== 'undefined' &&
                this.props.buttonColumnOptions.configurations.length > 0
            ) {
                let buttonsToReturn: Array<JSX.Element> = [];
                for (
                    let i = 0;
                    i < this.props.buttonColumnOptions.configurations.length;
                    i++
                ) {
                    let config = options.configurations[i];
                    if (
                        config.shouldDisplay === null ||
                        typeof config.shouldDisplay === 'undefined' ||
                        config.shouldDisplay(dataPoint)
                    ) {
                        buttonsToReturn.push(
                            <IconButton
                                {...config}
                                key={encodeURIComponent(
                                    JSON.stringify({
                                        __key__: i,
                                        ...dataPoint
                                    })
                                )}
                                data={encodeURIComponent(
                                    JSON.stringify(dataPoint)
                                )}
                            ></IconButton>
                        );
                    }
                }
                return buttonsToReturn;
            } else {
                return null;
            }
        };

        return (
            <div className={this.props.styleOptions.outerDivClass}>
                <div
                    className={this.props.styleOptions.headerDivClass}
                    style={
                        this.props.title === null ||
                        typeof this.props.title === 'undefined' ||
                        this.props.title === ''
                            ? ODataTableStyles.cardHeader
                            : ODataTableStyles.titledCardHeader
                    }
                >
                    {this.props.title ? (
                        <h4
                            className={this.props.styleOptions.titleClass}
                            style={{
                                ...ODataTableStyles.cardTitle,
                                ...ODataTableStyles.cursorPointer
                            }}
                            onClick={this.titleClick}
                        >
                            {this.props.title}
                        </h4>
                    ) : null}
                    <div style={ODataTableStyles.filterDiv}>
                        <input
                            ref={this._filterSearchInputRef}
                            style={ODataTableStyles.filterInput}
                            placeholder='&#x1f50d;  Search'
                            onChange={this.filterSearchChanged}
                        />
                    </div>
                    <div style={ODataTableStyles.alignJustifyCenter}>
                        {this._pagingButtonsTop ? pagingButtons : null}
                    </div>
                </div>
                <div className={this.props.styleOptions.bodyDivClass}>
                    {this.state.loaded &&
                    this.state.data &&
                    this.state.data.length > 0 ? (
                        <table
                            className={this.props.styleOptions.tableClass}
                            style={ODataTableStyles.oDataTableTable}
                        >
                            <thead>
                                <tr>
                                    {this.props.buttonColumnOptions &&
                                    this.props.buttonColumnOptions.position ===
                                        'first' ? (
                                        <th></th>
                                    ) : null}
                                    {this.props.columns.map(c => (
                                        <th
                                            key={encodeURIComponent(
                                                JSON.stringify(c)
                                            )}
                                            onClick={() =>
                                                this.thClickHandler(c)
                                            }
                                            style={
                                                c.sortable
                                                    ? ODataTableStyles.linkText
                                                    : {}
                                            }
                                        >
                                            {c.sortable && c.index != null
                                                ? c.name +
                                                  ` ${this.state.columnSortStates[
                                                      c.index
                                                  ].getState()}`
                                                : c.name}
                                        </th>
                                    ))}
                                    {this.props.buttonColumnOptions &&
                                    this.props.buttonColumnOptions.position ===
                                        'last' ? (
                                        <th></th>
                                    ) : null}
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.data.map(d => (
                                    <tr
                                        key={encodeURIComponent(
                                            JSON.stringify(d)
                                        )}
                                    >
                                        {this.props.buttonColumnOptions &&
                                        this.props.buttonColumnOptions
                                            .position === 'first' ? (
                                            <td>
                                                {generateButtonColumnButtons(
                                                    this.props
                                                        .buttonColumnOptions,
                                                    d
                                                )}
                                            </td>
                                        ) : null}
                                        {this.props.columns.map(c => (
                                            <td
                                                key={encodeURIComponent(
                                                    JSON.stringify(
                                                        Object.assign(c, d)
                                                    )
                                                )}
                                            >
                                                {
                                                    //display strings/numbers/booleans
                                                    c.objectKey &&
                                                    c.type !== 'enum' &&
                                                    c.type !== 'datetime'
                                                        ? String(
                                                              d[c.objectKey]
                                                          ) === 'null'
                                                            ? ''
                                                            : String(
                                                                  d[c.objectKey]
                                                              )
                                                        : //display string/number enums
                                                        c.objectKey &&
                                                          c.type === 'enum' &&
                                                          c.enum
                                                        ? c.enum[
                                                              Number(
                                                                  d[c.objectKey]
                                                              )
                                                          ] ??
                                                          //for a string serialized enum
                                                          String(d[c.objectKey])
                                                        : //display datetimes
                                                        c.objectKey &&
                                                          c.type ===
                                                              'datetime' &&
                                                          d[c.objectKey] !==
                                                              null &&
                                                          typeof d[
                                                              c.objectKey
                                                          ] !== 'undefined'
                                                        ? new Date(
                                                              d[c.objectKey]
                                                          ).toLocaleString()
                                                        : ''
                                                }
                                            </td>
                                        ))}
                                        {this.props.buttonColumnOptions &&
                                        this.props.buttonColumnOptions
                                            .position === 'last' ? (
                                            <td>
                                                {generateButtonColumnButtons(
                                                    this.props
                                                        .buttonColumnOptions,
                                                    d
                                                )}
                                            </td>
                                        ) : null}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    ) : !this.state.loaded ? (
                        <Skeleton
                            height={this._loadingSkeletonHeight}
                        ></Skeleton>
                    ) : (
                        <div
                            style={{
                                ...ODataTableStyles.textCenter,
                                ...ODataTableStyles.textGray
                            }}
                        >
                            No Data
                        </div>
                    )}
                </div>
                {this._hasFooterDiv ? (
                    <div className={this.props.styleOptions.footerDivClass}>
                        <div
                            style={{
                                ...ODataTableStyles.floatRight,
                                ...ODataTableStyles.w25
                            }}
                        >
                            {!this._pagingButtonsTop ? pagingButtons : null}
                        </div>
                    </div>
                ) : null}
            </div>
        );
    }
}

/**
 * Renders ODataTable to users specifications based on parentId and props
 * @param {string} parentId - the id of the div you want to put the ODataTable in
 * @param {IODataTableProps} props - ODataTable props
 */
export const renderODataTable = (parentId: string, props: IODataTableProps) =>
    ReactDOM.render(
        <ODataTable {...props} />,
        document.getElementById(parentId)
    );

/**
 * Extracts row data stored in user defined button configurations
 * @param {React.MouseEvent<HTMLButtonElement>} e
 * @returns {any | null} json serialized table row
 */
export const extractRowDataFromCustomButtonClickEvent = (
    e?: React.MouseEvent<HTMLButtonElement>
): any | null => {
    if (e) {
        let target: HTMLElement = e.target as HTMLElement;
        while (target.tagName.toLowerCase() !== 'button') {
            target = target.parentNode as HTMLElement;
        }
        var customButtonData = target.getAttribute('data-custom-button-data');
        if (customButtonData) {
            return JSON.parse(decodeURIComponent(customButtonData));
        }
        return null;
    }
};

/**
 * Extracts row data then returns rowData.Id
 * @param {React.MouseEvent<HTMLButtonElement>} e
 * @returns
 */
export const extractIdFromCustomButtonClickEvent = (
    e?: React.MouseEvent<HTMLButtonElement>
): any | null => extractRowDataFromCustomButtonClickEvent(e)?.Id;

interface IReturnMouseEventHandler {
    (e: React.MouseEvent<HTMLButtonElement>): void;
}

const formatUrl = (
    url: string,
    idToGoTo: number,
    queryParam: boolean = false
): string =>
    `${url}${
        queryParam
            ? `?id=${encodeURIComponent(idToGoTo)}`
            : `/${encodeURIComponent(idToGoTo)}`
    }`;

/**
 * Generates onMouseDown handler for button configurations that
 * makes the button act as if it were a link when middle-clicked
 * @param url the url you want the new window to open to, NO TRAILING SLASH
 * @param queryParam should the extracted id be appended as a queryParam? Default false (appends like route param)
 * @returns a function that ODataTableLib will use onMouseDown
 */
export const middleClickLinkBehaviorFunctionGenerator =
    (url: string, queryParam: boolean = false): IReturnMouseEventHandler =>
    (e: React.MouseEvent<HTMLButtonElement>): void => {
        if (e.button === 1) {
            const idToGoTo = extractIdFromCustomButtonClickEvent(e);
            window.open(formatUrl(url, idToGoTo, queryParam));
        }
    };

/**
 * Generates onClick handler for button configurations that makes the
 * button act as if it were a link and redirects to a new page
 * @param url
 * @param queryParam
 * @returns
 */
export const buttonClickLinkBehaviorFunctionGenerator =
    (url: string, queryParam: boolean = false): IReturnMouseEventHandler =>
    (e: React.MouseEvent<HTMLButtonElement>): void => {
        const idToGoTo = extractIdFromCustomButtonClickEvent(e);
        window.location.href = formatUrl(url, idToGoTo, queryParam);
    };

/**
 * globalThis library
 * @object
 */
const defaultExport = {
    render: renderODataTable,
    faEdit: faEdit,
    faTrash: faTrash,
    faList: faList,
    faStar: faStar,
    faCopy: faCopy,
    extractRowDataFromCustomButtonClickEvent:
        extractRowDataFromCustomButtonClickEvent,
    extractIdFromCustomButtonClickEvent: extractIdFromCustomButtonClickEvent,
    middleClickLinkBehaviorFunctionGenerator:
        middleClickLinkBehaviorFunctionGenerator,
    buttonClickLinkBehaviorFunctionGenerator:
        buttonClickLinkBehaviorFunctionGenerator
};
export default defaultExport;
