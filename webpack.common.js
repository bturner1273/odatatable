const path = require('path');

module.exports = {
    entry: './src/ODataTable.tsx',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader']
            }
        ],

    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
        filename: 'ODataTable.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/build',
        library: {
            name: 'ODataTableLib',
            type: 'global',
            export: 'default'
        }
    },
    cache: false
};
