#### Example Usage:
```html
<html>
    <head>
        <!-- Bootstrap CSS import for styling the ODataTable component-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <div id="root" class="container-fluid p-5"></div>
        <!-- Bootstrap JS import for styling the ODataTable component -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <!-- ODataTable import -->
        <script src="./path/to/dist/ODataTable.js"></script>
        <script>
            window["ODataTableLib"]
            .render("root", {
            title: 'Products',
            indexUrl: "http://localhost:5000/odata/products/",
            columns: [
                {
                    name: 'Name',
                    sortable: true,
                    type: 'string'
                },
                {
                    name: 'Description',
                    sortable: true,
                    type: 'string'
                },
                {
                    name: 'Key',
                    sortable: true,
                    type: 'string'
                },
                {
                    name: 'Category',
                    objectKey: 'CategoryId',
                    sortable: true,
                    type: 'enum',
                    enum: {
                        1: 'Portable Batteries',
                        2: 'Chargers',
                        3: 'Charger Adapters',
                        4: 'Other',
                        5: 'Cable & Harness'
                    }
                },
                {
                    name: 'CreatedAt',
                    sortable: true,
                    type: 'datetime'
                },
                {
                    name: 'IsAutoCreated',
                    sortable: true,
                    type: 'boolean'
                }
            ],
            multiColumnFilterEnabled: true,
            styleOptions: {
                outerDivClass: 'card text-white bg-dark',
                headerDivClass: 'card-header',
                titleClass: 'card-title',
                bodyDivClass: 'card-body table-responsive',
                hasFooterDiv: false,
                footerDivClass: 'card-footer',
                tableClass: 'table table-sm table-dark table-striped table-bordered'
            },
            pagingOptions: {
                pageSize: 50,
                pagingDescriptionSpanClass: 'text-muted font-weight-light',
                pagingButtonsVertical: false,
                pagingButtonsTop: true
            },
            buttonColumnOptions: {
                position: 'last',
                configurations: [
                    {
                        text: 'Edit',
                        icon: window['ODataTableLib'].faEdit,
                        onClick: (e) => {
                            console.log('Edit clicked');
                        },
                        buttonClass: 'btn btn-sm btn-warning mx-1'
                    },
                    {
                        text: 'Delete',
                        icon: window['ODataTableLib'].faTrash,
                        onClick: (e) => {
                            console.log('Delete clicked');
                        },
                        buttonClass: 'btn btn-sm btn-danger mx-1'
                    }
                ]
            }});
        </script>
    </body>
</html>
```
If used with an OData compliant Manufacturing Products server route, this client side code produces:

![](/readme_assets/images/products_odatatable.png)


For the Typedoc either download the "compile_docs" artifact from the repositories main page or clone the repo. Open the downloaded artifact/repo in Visual Studio Code and go to docs/index.html. Right click on index.html and choose the 'Open with Live Server' option.